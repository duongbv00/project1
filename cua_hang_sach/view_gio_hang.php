<link rel="stylesheet" type=text/css href="../Display/header.css">
<link rel="stylesheet" type=text/css href="../Display/footer.css">
<?php require'../Display/header-gio-hang.php';?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type=text/css href="gio_hang.css">
</head>
<body>
<div id="kv-home">
		
</div>
<div class="div-gh">
	<?php
		session_start();
		if(empty($_SESSION['maTK'])){
			header('location:../tai_khoan/login/login.php');
		}else{
			if(empty($_SESSION['gio_hang'])){
				echo "bạn chưa có sản phẩm";
			}else{
				$gio_hang = $_SESSION['gio_hang'];
				?>
				<h3> Giỏ hàng của bạn</h3>
				<br>
				<br>
				<a href="cua_hang_sach.php">tiếp tục đặt hàng</a>
				 <table border="1" width="100%">
				 	<tr>
				 		<th>Tên Sách</th>
				 		<th> Ảnh</th>
				 		<th> Số lượng </th>
				 		<th> Giá </th>
				 		<th>Thành tiền</th>
				 		<th>Xóa</th>
				 	</tr>
				 	<?php foreach ($gio_hang as $ma_san_pham => $san_pham): ?>
				 	<tr>
				 		<td>
				 			<?php echo $san_pham['tenSP'] ?>
				 		</td>
				 		<td>
				 			<img width="150px;"height = "200px" src="<?php echo $san_pham['anhSP'] ?>">
				 		</td>
				 		<td>
				 			<a href="giam_so_luong.php?maSP=<?php echo $ma_san_pham ?>">-</a>
				 			<?php echo $san_pham['soLuong'] ?>
				 			<a href="tang_so_luong.php?maSP=<?php echo $ma_san_pham ?>">+</a>
				 		</td>
				 		<td>
				 			<?php echo $san_pham['GiaSP'] ?>
				 		</td>
				 		<td>
				 			<?php echo $san_pham['GiaSP']*$san_pham['soLuong'] ?>
				 		</td>
				 		<td>
				 			<a href="xoa_tung_san_pham.php?maSP=<?php echo $ma_san_pham ?>">xóa</a>
				 		</td>
				 	</tr>
				 	<?php endforeach ?>
					</table>
				<?php }
		}
	?>
<br>
<span class="a"><a href="delete_gio_hang.php">Xóa tất cả</a></span>
<br>
<br>
<hr>
<br>
<h3>Đặt hàng</h3>
<form action="process_dat_hang.php" method="post" class="form-gh">
	Tên người nhận
	<input type="text" name="ten_nguoi_nhan">
	<br>
	Số điện thoại
	<input type="text" name="SDT">
	<br>
	Địa chỉ người nhận
	<textarea name="dia_chi"></textarea>
	<br>
	<button>Đặt hàng</button>
</form>
</div>
<div class="in-footer"><?php require'../Display/footer.php';?></div>
</body>
</html>