<link rel="stylesheet" type=text/css href="../Display/header.css">
<link rel="stylesheet" type=text/css href="../Display/footer.css">
<?php require'../Display/header_cua_hang_sach.php';?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="cua_hang_sach.css">
</head>
<body>
	<?php
		require'../connect.php';
		$sql = "select*from san_pham";
		$result= mysqli_query($connect,$sql);
		$num = mysqli_num_rows($result);
		$trang_hien_tai=1;
		if(isset($_GET['so_trang'])){
			$trang_hien_tai = $_GET['so_trang'];
		}
		$so_bai_tren_1_trang = 12;
		$so_trang = ceil($num / $so_bai_tren_1_trang);
		$bo_qua = ($trang_hien_tai - 1) * $so_bai_tren_1_trang;
		$sql = "select*from san_pham limit $so_bai_tren_1_trang offset $bo_qua";
		$result= mysqli_query($connect,$sql);
		
	 ?>
	<div class="style-kv">
		<div class ="kv1"></div>
		<div class="kv2">
			<div class="div1">&ensp;&ensp;SÁCH HAY</div>
		</div>
		<div class ="kv3">
			<div>CỬA HÀNG</div>
			<div class= "pt-css">
				<div id ="menu-pt">
					<ul>	
						<?php for(
							$i = 1;
							$i<=$so_trang;$i++){ ?>
							<li>
				 				<a href="?so_trang=<?php echo $i ?>">
				 					<div class="opt">
				 						<?php echo $i; ?>
				 					</div>
				 				</a>
			 				</li>
			 			<?php } ?>
			 		</ul>
				</div>
			</div>
			<?php foreach ($result as $each): ?>
				<div class ="style-SP">
					<span class="ct-sp">
						<img id ='img-s' width="200px" height="300px" src="<?php echo $each['anhSP']; ?>">
						<br>
						<span class = "div-t">
							<a href="san_pham.php?msp=<?php echo $each['maSP']; ?>"> 
								<?php echo $each['tenSP'];?>
							</a>
						</span>
						<br>
						<span class="div-g"> 
							<span>
								<?php 
							    echo number_format($each['GiaSP']);
								?>
							</span>
							<span>₫</span>
						</span>
						<br>
						<span class="mua">
							<a href="gio_hang.php?maSP=<?php echo $each['maSP']?>">
								<p>Thêm giỏ hàng</p>
							</a>
						</span>
					</span>
				</div>
			<?php endforeach ?>
			<div class= "pt-css">
				<div id ="menu-pt">
					<ul>	
						<?php for($i = 1;$i<=$so_trang;$i++){ ?>
							<li>
				 				<a href="?so_trang=<?php echo $i ?>">
				 					<div class="opt">
				 						<?php echo $i ?>
				 					</div>
				 				</a>
			 				</li>
			 			<?php } ?>
			 		</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="in-footer"><?php require'../Display/footer.php';?></div>
</body>
</html>