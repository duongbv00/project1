<link rel="stylesheet" type=text/css href="../Display/header.css">
<link rel="stylesheet" type=text/css href="../Display/footer.css">
<?php require'../Display/header_cua_hang_sach.php';?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type=text/css href="san_pham.css">
</head>
<body>
	<?php 
		$msp = $_GET['msp'];
		include '../connect.php';
		$sql="select * from quan_ly_the_loai";
		$result=mysqli_query($connect,$sql);

		$sql="select * from san_pham inner join quan_ly_the_loai on san_pham.ma_the_loai = quan_ly_the_loai.ma_the_loai where maSP = '$msp'";
		$kq=mysqli_query($connect,$sql);

		mysqli_close($connect);
	?>
	<div class="kv1"></div>
	<div class ="header-sp">
		<div class="menu-tl">
			<div class="dms">
 				<p class="dms">
 					DANH MỤC SÁCH
 				</p>
			</div>
			<div class="dmtl">
				<?php foreach ($result as $each): ?>
	 					<ul>
	 						<li>
		 						<a href="">
									<div>
										<?php echo $each['ten_the_loai'];?>
									</div>
								</a>
							</li>
	 					</ul>
	 				<?php endforeach?>
			</div>
		</div>
		<div class="tttl">
			<div class="anh">
				<?php foreach ($kq as $sp): ?>
					<img class = "img1" width="420px" height="500px" src="<?php echo $sp['anhSP']?>">
				<?php endforeach?>
			</div>
		</div>
		<div class="tt-sp">
			<?php foreach ($kq as $sp): ?>
			<div class="gt-sp">
				 <p><?php echo $sp['tenSP'];?></p>
				 <br>
				 <br>
				 <br>
				 <span>
				 	TÌNH TRẠNG:  
				 	<span class="tc">
				 		<?php
					 		if($sp['soLuong']==0){
					 			echo " HẾT HÀNG";
					 		} 
					 		else{
					 			echo " CON HÀNG";
					 		}
				 		?>
				 	</span>
				 </span>
			</div>
			<div class = "tt-ct">
				<table class="table-sp">
					<tr>
						<td>Thể loại</td>
						<td>
							<?php echo $sp['ten_the_loai']?>
						</td>
					</tr>
					<tr>
						<td>Tác giả</td>
						<td>
							<?php echo $sp['tacGia']?>
						</td>
					</tr>
					<tr>
						<td>Nhà xuất bản</td>
						<td>
							<?php echo $sp['nhaXB']?>
						</td>
					</tr>
					<tr>
						<td>Năm xuất bản</td>
						<td>
							<?php echo $sp['namXB']?>
						</td>
					</tr>
				</table>
				<p class = "giaSP">
					<span>Giá:</span>
					<?php 
						echo number_format($sp['GiaSP']);
					?>
					<span>₫</span>
				</p>
				<div class="gh">
					<p>Thêm vào giỏ hàng</p>
				</div>
			</div>
			<?php endforeach?>
		</div>
		<div class="mo_ta">
			<p>MÔ TẢ</p>
			<?php foreach ($kq as $sp): ?>
			<div class="mo_ta2">
				<p>
					<?php echo $sp['tenSP'];?>	
				</p>
				<br>
				<span>
					<?php echo $sp['moTa']?>
				</span>
				<?php endforeach?>
			</div>
		</div>

	</div>
<div class="in-footer"><?php require'../Display/footer.php';?></div>
</body>
</html>