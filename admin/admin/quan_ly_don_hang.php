<?php require"header_don_hang.php"?>
<link rel="stylesheet" type=text/css href="header_admin.css">
<link rel="stylesheet" type=text/css href="../../Display/header.css">
<link rel="stylesheet" type=text/css href="../../cua_hang_sach/view_gio_hang.php">
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
 <?php 
	require '../../connect.php';

	$sql = "select * from hoa_don_chi_tiet inner join hoa_don on hoa_don_chi_tiet.ma_hoa_don = hoa_don.ma_hoa_don inner join san_pham on hoa_don_chi_tiet.ma_san_pham = san_pham.maSP";
	$result = mysqli_query($connect,$sql);
?>
	<div class="div-gh">
 		<h3>Đơn hàng của bạn.</h3>
 		<br>
 		<br>
		<table border="1" width="100%">
			<tr>
				<th>mã hóa đơn</th>
				<th> tên sản phẩm </th>
				<th>Ảnh sản phẩm </th>
				<th>Tên người nhận</th>
				<th>Số điện thoại</th>
				<th>Địa chỉ</th>
				<th>số lượng</th>
				<th>Thời gian đặt</th>
				<th>giá</th>
				<th>Tình trạng</th>
			</tr>
			<?php foreach ($result as $each): ?>
			<tr>
				 <td>
				 	<?php echo $each['ma_hoa_don'];?>
				 </td>
				<td><?php echo $each['tenSP'];?> </td>
				<td>
					<img width="150px;"height = "200px" src="<?php echo $each['anhSP'] ?>">
				 </td>
				 <td>
				 	<?php echo $each['ten_nguoi_nhan'];?>
				 </td>
				 <td>
				 	<?php echo $each['so_dien_thoai'];?>
				 </td>
				 <td>
				 	<?php echo $each['dia_chi'];?>
				 </td>
				 <td>
				 	<?php echo $each['so_luong'];?>
				 </td>
				 <td>
				 	<?php echo $each['thoi_gian_dat'];?>
				 </td>
				 <td>
				 	<?php echo $each['gia'];?>
				 </td>
				<td>
					<?php 
 					if($each['tinh_trang']==0){
 						echo "Đã duyệt";
 					}
 					if($each['tinh_trang']==1){
 					 ?>
 						<a href="duyet.php?ma_hoa_don=<?php echo $each['ma_hoa_don']?>">Duyệt đơn hàng</a>
 					<?php }
 				?>
				 </td>
			</tr>
			<?php endforeach ?>
		</table>
 	</div>
</body>
</html>