<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type=text/css href="footer.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	<div class="footer">
		<div class="footer-top">
			<div class ="fa-sty">
				<div>
					<a href="https://www.facebook.com/ngoc.duong.23122000">
							<i class="fa fa-facebook"></i>
					</a>
				</div>
			</div>
		</div>
		<div class = "footer-center">
			<div class="div-ct">
				<div class="span-1">
					<div>
						<img src="http://tramdoc.vn/themes/test_desktop/images/logo.png">
						<p>Với sứ mệnh chính là “Tôn vinh giá trị tri thức” Việt Nam.Chúng tôi luôn đành niềm đam mê cập nhật các thể loại sách hay, sách mới, những quyển sách có giá trị thức cao cho khách hàng.</p>
					</div>
				</div>
				<div class = "span-2">
					<div>
						<p>
							Thành viên:
							<span>Chu Ngọc Dương</span>
						</p>
						<p>
							lớp:
							<span>BKD04</span>
						</p>
						<p>
							Dề tài:
							<span>Đồ án 1 - web bán sách</span>
						</p>
						<p>
							Địa chỉ:
							<span>viện CNTT bách khoa (BKACAD)</span>
						</p>
						<p>
							Email:
							<span>duongbv00@gmail.com</span>
						</p>
						<p>
							Điện thoại:
							<span>0376.802.312</span>
						</p>
					</div>
					<div class="span-3">
						<p>Facebook</p>
						<a href="https://www.facebook.com/ngoc.duong.23122000">
							<img src="https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/80239680_895863554222998_9196526342842791530_n.png?_nc_cat=109&_nc_sid=b96e70&_nc_oc=AQlpXVHM3Dycw5LLSDQHAd2RdsbvKGVczlSmbh5owAJks9rHvOCg7USwS9g8Ejgt2hqKtmo-GpVWzRjlybvRVTWt&_nc_ht=scontent.fhan2-3.fna&oh=c732e744e1bcd0dbfd828bd7e3331f58&oe=5F1F583E">
						</a>
					</div>
					<div class="span-4">
						<p> Danh Mục Web </p>
						<p><a href=""> Cửa hàng </a></p>
						<p><a href=""> Tài khoản </a></p>
						<p><a href=""> Tin sách</a></p>
						<p><a href=""> Liên hệ </a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
				
		</div>
	</div>
</body>
</html>