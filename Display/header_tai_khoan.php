
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
	<?php 
		require'../../connect.php';
		$tim_kiem = '';
		if(isset($_GET['tim_kiem'])){
			$tim_kiem = $_GET['tim_kiem'];
		}

		$sql= "select * from san_pham where tenSP like '%%'";
		$result = mysqli_query($connect,$sql);
	?>
	<div class="header-style">
		<header class="header-1">
			<div class="header-top">
				<div >
					<?php 
						session_start();
						if(empty($_SESSION['maTK'])){
							
						}
						else{
							echo "<div class='login'>";
							echo " Xin chào: ".$_SESSION['hoTen'];
							echo "<a href='../../dang_xuat.php'>";
							echo "   Thoát";
							echo "</a>";
							echo "</div>";
						}
					?>
				</div>
			</div>

			<div class="header-center">
				<div class="hd-ct">
					<img src="http://tramdoc.vn/themes/test_desktop/images/logo.png">
					<form class="div-search" action="../../search.php">
						<input id="search-style" type="search" name ="tim_kiem" placeholder="Tìm sách nhanh nhất..." value="<?php echo $tim_kiem?>">
						<div class="icon-search-style">
							<button id="icon-search"><i class="fas fa-search"></i></button>
						</div>
				    </form>
				    <div class = "div-lh">
				    	<i class="fa fa-user"></i>
				    	<span>LIÊN HỆ NGAY</span>
				    	<p>0376.802.312|24/24</p>
				    </div>
			    <div>
			</div>

			<div class="header-menu">
				<div id ="menu">
					<ul>
						<li>
							<a href="../../index.php">
								<div>
									<i class="fas fa-home"></i>
									TRANG CHỦ
								</div>
							</a>
						</li>
						<li>
							<a href="../../cua_hang_sach/cua_hang_sach.php">
								<div>
									<i class="fas fa-shopping-cart"></i>
									CỬA HÀNG SÁCH
								</div>
							</a>
						</li>
						<li style="background-color:#787878;">
							<a href="login.php">
								<div >
									<i class="fas fa-user"></i>
									TÀI KHOẢN
								</div>
							</a>
						</li>
					</ul>
				</div>
				<!-- giỏ hàng -->
				<div class="gh-div">
					<div id ='nz-div-2'>
						<a href="../../cua_hang_sach/view_gio_hang.php">
							<div class="tde" >
								<span>
									<i class="fas fa-shopping-basket"></i>
								 	GIỎ HÀNG
								</span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</header>
	</div>
</body>
</html>