<link rel="stylesheet" type=text/css href="Display/header.css">
<link rel="stylesheet" type=text/css href="Display/footer.css">
<?php require'Display/header-home.php';?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 	<link rel="stylesheet" type=text/css href="index.css">
 </head>
 <body>
 	<div class = "style-home">
 		<div class="bn-st">
	 		<div class="menu-style">
	 			<div class="menu-dm">
	 				<div class="icon">
	 					<p class="icon-menu"><i class="fas fa-align-justify"></i></p>
	 				</div>
	 				<p class="dms">
	 					DANH MỤC SÁCH
	 				</p>
	 			</div>
	 			<div class="tl-style">
		 			<div id = "menu-dm">
		 				<?php 
		 					include 'connect.php';
		 					$sql="select * from quan_ly_the_loai";
		 					$result=mysqli_query($connect,$sql);
		 					$sql="select * from san_pham inner join quan_ly_the_loai on san_pham.ma_the_loai = quan_ly_the_loai.ma_the_loai";
		 					$kq=mysqli_query($connect,$sql);
		 					mysqli_close($connect);
		 				?>

		 				<?php foreach ($result as $each): ?>
		 					<ul>
		 						<li>
			 						<a href="lay_tat_ca_san_pham_theo_the_loai.php?ma_the_loai=<?php echo $each['ma_the_loai'] ?>">
										<div>
											<?php echo $each['ten_the_loai'];?>
										</div>
									</a>
								</li>
		 					</ul>
		 				<?php endforeach?>
		 			</div>
		 		</div>
	 		</div>
	 	</div>
	 	<div>	
	 		<div class="hua">
	 			<ul class="slides">
				    <input type="radio" name="radio-btn" id="img-1" checked />
				    <li class="slide-container">
				        <div class="slide">
				            <img style="width: 870px; height: 350px;" src="https://muasachhay.vn/wp-content/uploads/2018/10/xay-dung-doanh-nghiep-hoan-hao-mua-sach-hay.jpg" />
				        </div>
				        <div class="nav">
				            <label for="img-6" class="prev">&#x2039;</label>
				            <label for="img-2" class="next">&#x203a;</label>
				        </div>
				    </li>

				    <input type="radio" name="radio-btn" id="img-2" />
				    <li class="slide-container">
				        <div class="slide">
				          <img style="width: 870px; height: 350px;" src="https://muasachhay.vn/wp-content/uploads/2016/08/einstein-cuoc-doi-va-vu-tru-banner-mua-sach-hay.jpg" />
				        </div>
				        <div class="nav">
				            <label for="img-1" class="prev">&#x2039;</label>
				            <label for="img-3" class="next">&#x203a;</label>
				        </div>
				    </li>

				    <input type="radio" name="radio-btn" id="img-3" />
				    <li class="slide-container">
				        <div class="slide">
				          <img style="width: 870px; height: 350px;" src="https://muasachhay.vn/wp-content/uploads/2018/10/sach-thich-nhat-hanh-mua-sach-hay.jpg" />
				        </div>
				        <div class="nav">
				            <label for="img-2" class="prev">&#x2039;</label>
				            <label for="img-4" class="next">&#x203a;</label>
				        </div>
				    </li>

				    <input type="radio" name="radio-btn" id="img-4" />
				    <li class="slide-container">
				        <div class="slide">
				          <img style="width: 870px; height: 350px;" src="https://muasachhay.vn/wp-content/uploads/2018/10/mua-sach-hay-sach-hay-tinh-tuyen.png" />
				        </div>
				        <div class="nav">
				            <label for="img-3" class="prev">&#x2039;</label>
				            <label for="img-5" class="next">&#x203a;</label>
				        </div>
				    </li>
				    <li class="nav-dots">
				      <label for="img-1" class="nav-dot" id="img-dot-1"></label>
				      <label for="img-2" class="nav-dot" id="img-dot-2"></label>
				      <label for="img-3" class="nav-dot" id="img-dot-3"></label>
				      <label for="img-4" class="nav-dot" id="img-dot-4"></label>
				    </li>
				</ul>
	 		</div>
	 		<div class="img-div">
	 			<img class="img-c" src="https://muasachhay.vn/wp-content/uploads/2014/12/book_massive.jpg">
	 		</div>
	 	</div>
	 	<div class="img-div1">
	 		<img src="https://muasachhay.vn/wp-content/uploads/2014/12/free-shipping.png">
	 		<img src="https://scontent.fhan2-1.fna.fbcdn.net/v/t1.15752-9/106359854_332393357753723_5268653445342896097_n.png?_nc_cat=102&_nc_sid=b96e70&_nc_oc=AQmIOV9Eie_gecslSyFTo3fl7xUFETVkIBWdqv1r7SHPRpOBYSMzTaUmuMFh3-8vH5t524z6p44YB5SMP4DLg-GY&_nc_ht=scontent.fhan2-1.fna&oh=aaa0389613825ea08c4d7b6e59b4ab02&oe=5F1FB8FA">
	 		<img src="https://muasachhay.vn/wp-content/uploads/2014/12/deal-1.png">
	 	</div>
 		<div class="div-content">
 			<?php require'content.php';?>
 		</div>
 		<div class="div-img2">
 			<img src="https://muasachhay.vn/wp-content/uploads/2016/02/logo-nxb-tre-chuan-med-.jpg">
 			<img src="https://muasachhay.vn/wp-content/uploads/2016/02/nxb.logo_.png">
 			<img src="https://muasachhay.vn/wp-content/uploads/2016/02/slogan_logo-Nha-Nam.jpg">
 			<img src="https://muasachhay.vn/wp-content/uploads/2016/02/logo-tri-thuc.jpg">
 			<img src="https://muasachhay.vn/wp-content/uploads/2016/02/images.png">
 		</div>
 	</div>
 	<div class="tr-footer">
 		<?php require'Display/footer.php';?>
 	</div>
 </body>
 </html>
